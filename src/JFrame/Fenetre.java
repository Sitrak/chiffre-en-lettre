package JFrame;

import java.awt.*;
import javax.swing.*;

public class Fenetre extends JFrame {
    JButton b1, b2, b3;
    public Fenetre() {
        Container c = getContentPane();
        c.setLayout(new FlowLayout());
        b1 = new JButton("Salut");
        b2 = new JButton("Ok");
        b3 = new JButton("Quitter");

        c.add(b1);
        c.add(b2);
        c.add(b3);

        setSize(400, 400);
        setTitle("Exemple");
        setVisible(true);
    }
    public static void main(String[] args) {
        Fenetre f = new Fenetre();
    }
}