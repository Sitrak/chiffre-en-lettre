package JFrame;

import java.awt.*;
import javax.swing.*;

public class Layout extends JFrame{

    JTextField z1, z2;
    JButton b1;

    public Layout() {
        Container c = getContentPane();
        c.setLayout(null);
        
        z1 = new JTextField();
        z2 = new JTextField();
        
        b1 = new JButton("Calculer");
        z1.setBounds(150, 150, 150, 50);
        z2.setBounds(150, 150, 150, 50);
        b1.setBounds(150, 250, 150, 50);
        c.add(z1);
        c.add(z2);
        c.add(b1);
        
        setSize(400,300);
        setTitle("Test");
        setVisible(true);
    }
    
    public static void main(String[] args) {
        Layout l = new Layout();
    }

}