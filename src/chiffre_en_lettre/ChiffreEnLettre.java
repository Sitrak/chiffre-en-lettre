
package chiffre_en_lettre;
public class ChiffreEnLettre {
	// 1 à 9
	static String unite(long x) {
		String lettre = "";
		if (x == 1) lettre = "un";
		else if (x == 2) lettre = "deux";
		else if (x == 3) lettre = "trois";
		else if (x == 4) lettre = "quatre";
		else if (x == 5) lettre = "cinque";
		else if (x == 6) lettre = "six";
		else if (x == 7) lettre = "sept";
		else if (x == 8) lettre = "huit";
		else if (x == 9) lettre = "neuf";
		return lettre.toUpperCase();
	}
	// 10 à 99
	static String dizaine(long x) {
		String lettre = "";
		if (x == 10) lettre = "dix";
		else if (x == 11) lettre = "onze";
		else if (x == 12) lettre = "douze";
		else if (x == 13) lettre = "treize";
		else if (x == 14) lettre = "quatorze";
		else if (x == 15) lettre = "quinze";
		else if (x == 16) lettre = "seize";
		else if (x == 17) lettre = "dix-sept";
		else if (x == 18) lettre = "dix-huit";
		else if (x == 19) lettre = "dix-neuf";
		else if ((x >= 20) && (x < 30)) lettre = "vinght " + unite(x - 20);
		else if ((x >= 30) && (x < 40)) lettre = "trente " + unite(x - 30);
		else if ((x >= 40) && (x < 50)) lettre = "quarente " + unite(x - 40);
		else if ((x >= 50) && (x < 60)) lettre = "cinquante " + unite(x - 50);
		else if ((x >= 60) && (x < 70)) lettre = "soixante " + unite(x - 60);
		else if ((x >= 70) && (x < 80)) lettre = "soixante " + dizaine(x - 60);
		else if ((x >= 80) && (x < 90)) lettre = "quatre vinght " + unite(x - 80);
		else if ((x >= 90) && (x <100))lettre = "quatre vinght " + dizaine(x - 80);
		return lettre.toUpperCase();
	}
	// 100 à 999
	static String centaine(long x) {
		String lettre = "";
		if ((x >= 100) && (x < 200)) lettre = "cent " + dizaine(x % 100) + unite(x % 100); // Ialana @le oe "un cent"
		else if ((x >= 200) && (x <1000)) lettre = unite (x / 100) + " cent " + dizaine(x % 100) + unite(x % 100);
		return lettre.toUpperCase();
	}
	// 1.000 à 999.999
	static String milier(long x) {
		String lettre = "";
		if ((x >= 1000) && (x < 2000)) lettre = "mille " + centaine(x % 1000) + dizaine(x % 1000) + unite(x % 1000); //Ialana @le oe "un mille"
		else if ((x >= 2000) && (x < 1000000))
			lettre = centaine(x / 1000) + dizaine(x / 1000) + unite(x / 1000) + " mille " 
					 + centaine(x % 1000) + dizaine(x % 1000) + unite(x % 1000);
		return lettre.toUpperCase();
	}
	// 1.000.000 à 999.999.999
	static String million(long x) {
		String lettre = "";
		if ((x >= 1000000) && (x < 1000000000)) 
			lettre = centaine(x / 1000000) + dizaine(x / 1000000) + unite(x / 1000000) + " million " 
					 + milier(x % 1000000) + centaine(x % 1000000) + dizaine(x % 1000000) + unite(x % 1000000);
		return lettre.toUpperCase();
	}
	// 1.000.000.000 à dix millions de milliards
	static String milliard(long x) {
		String lettre = "";
		if ((x >= 1000000000) && (x <= 10000000000000000L))
			lettre = million(x / 1000000000) + milier(x / 1000000000) + centaine(x / 1000000000) + dizaine(x / 1000000000) 
					+ unite(x / 1000000000) + " milliard " + million(x % 1000000000) + milier(x % 1000000000) 
					+ centaine(x % 1000000000) + dizaine(x % 1000000000) + unite(x % 1000000000);
		return lettre.toUpperCase();
	}
	/*public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		long ch;
		System.out.print("ENTRER LE NOMBRE A CONVERTIR : ");
		ch = sc.nextLong();
		if (ch == 0) 
			System.out.print("zéro");
		else if (ch < 10) 
			System.out.print(unite(ch));
		else if ((ch >= 10) && (ch < 100)) 
			System.out.print(dizaine(ch));
		else if ((ch >= 100) && (ch < 1000)) 
			System.out.print(centaine(ch));
		else if ((ch >= 1000) && (ch < 1000000)) 
			System.out.print(milier(ch));
		else if ((ch >= 1000000) && (ch < 1000000000)) 
			System.out.print(million(ch));
		else if ((ch >= 1000000000) && (ch <= 10000000000000000L)) 
			System.out.print(milliard(ch));	
		sc.close();
	}*/
}